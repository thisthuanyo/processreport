﻿ALTER PROCEDURE GetReportData
	@page INT,
	@pageSize INT
AS
BEGIN
	WITH DataCTE AS
	(
		SELECT 
			ROW_NUMBER() OVER (ORDER BY LOC.SubCompanyNameVN) - 1 AS RowNumber, -- Đánh số thứ tự (Trừ dòng grand total)

			TotalRecords = COUNT(*) OVER() - 1, -- Lấy tổng các dòng (Trừ dòng grand total)

			CASE WHEN GROUPING(LOC.BranchName) = 0
		    THEN 2
		    ELSE 1
		    END AS TypeBranch,
			
			CASE WHEN GROUPING(LOC.BranchName) = 0
		    THEN LOC.BranchName
		    ELSE LOC.SubCompanyNameVN
		    END AS SubCompanyOrBranchName,

			COUNT(LOC.BranchCode) as Total,

			----------- Chưa xử lý
			SUM (CASE WHEN SS.Status IN (5, 6) THEN 1 ELSE 0 END) AS CountNotProcessedYet,
			ROUND (CAST (SUM (CASE WHEN SS.Status IN (5, 6) THEN 1 ELSE 0 END) AS float) * 100 / COUNT(LOC.BranchCode), 2) 
			as [PercentNotProcessedYet],
		
			----------- Đang xử lý
			SUM (CASE WHEN SS.Status IN (3) THEN 1 ELSE 0 END) AS CountProcessing,
			ROUND (CAST (SUM (CASE WHEN SS.Status IN (3) THEN 1 ELSE 0 END) AS float) *100 /COUNT(LOC.BranchCode), 2)
			as [PercentProcessing],

			----------- Đã hoàn tất trong 5 ngày (Trừ chủ nhật)
			SUM (CASE WHEN SS.Status IN (8, 9) AND (DATEDIFF(day, SS.CreateDate, SS.EndDate) - DateDiff(ww, SS.CreateDate, SS.EndDate)) <= 5 
				THEN 1 ELSE 0 END) AS CountProcessedIn5Days,
			ROUND (CAST (SUM (CASE WHEN SS.Status IN (8, 9) AND (DATEDIFF(day, SS.CreateDate, SS.EndDate) - DateDiff(ww, SS.CreateDate, SS.EndDate)) <= 5 
				THEN 1 ELSE 0 END) AS float) * 100.0 / COUNT(LOC.BranchCode), 2) as [PercentProcessedIn5Days],

			----------- Đã hoàn tất quá 5 ngày (Trừ chủ nhật)
			SUM (CASE WHEN SS.Status IN (8, 9) AND (DATEDIFF(day, SS.CreateDate, SS.EndDate) - DateDiff(ww, SS.CreateDate, SS.EndDate)) > 5 
				THEN 1 ELSE 0 END) AS CountProcessedOver5Days,
			ROUND (CAST (SUM (CASE WHEN SS.Status IN (8, 9) AND (DATEDIFF(day, SS.CreateDate, SS.EndDate) - DateDiff(ww, SS.CreateDate, SS.EndDate)) > 5 
				THEN 1 ELSE 0 END) AS float) * 100.0 / COUNT(LOC.BranchCode), 2) as [PercentProcessedOver5Days]

		FROM DBO.WorkingSession AS SS
		JOIN DBO.Location AS LOC ON SS.LocationID = LOC.LocationID AND SS.BranchCode = LOC.BranchCode
		JOIN DBO.Status AS ST ON SS.Status = ST.ID
		GROUP BY ROLLUP(LOC.SubCompanyNameVN, LOC.BranchName)
		ORDER BY LOC.SubCompanyNameVN, LOC.BranchName
		OFFSET 1 ROWS -- Không lấy dòng grand total (ROLLUP)		
	)
	SELECT * FROM [DataCTE]
	WHERE RowNumber BETWEEN ((@page - 1) * @pageSize) + 1 AND (@page * @pageSize)
	
END

EXEC GetReportData 1, 10
GO
