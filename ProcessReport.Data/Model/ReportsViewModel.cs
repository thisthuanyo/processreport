﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessReport.Data.Model
{
    public class ReportsViewModel
    {
        public int TotalRecords { get; set; }
        public int TypeBranch { get; set; }
        public string SubCompanyOrBranchName { get; set; }
        public int? Total { get; set; }
        public int? CountNotProcessedYet { get; set; }
        public float? PercentNotProcessedYet { get; set; }
        public int? CountProcessing { get; set; }
        public float? PercentProcessing { get; set; }
        public int? CountProcessedIn5Days { get; set; }
        public float? PercentProcessedIn5Days { get; set; }
        public int? CountProcessedOver5Days { get; set; }
        public float? PercentProcessedOver5Days { get; set; }
    }
}
