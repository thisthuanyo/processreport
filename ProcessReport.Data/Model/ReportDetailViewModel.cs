﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProcessReport.Data.Model
{
    public class ReportDetailViewModel
    {
        public int TotalRecords { get; set; }
        public string SubCompanyNameVN{ get; set; }
        public string BranchName{ get; set; }
        public string Status{ get; set; }
        public DateTime CreateDate{ get; set; }
        public DateTime EndDate{ get; set; }

    }
}
