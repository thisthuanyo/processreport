﻿/*
            typeBranch: là vùng hay chi nhánh?
            1 : Vùng VD: Vùng 1, vùng 2,...
            2 : Chi nhánh (thuộc vùng) VD: HNI_02, HNI_07,...

            branchName: Tên vùng hoặc tên chi nhánh

            typeCondition: điều kiện
            0 : Tất cả
            1 : Chưa xử lý
            2 : Đang xử lý
            3 : Xử lý hoàn tất trong 5 ngày
            4 : Xử lý hòa tất quá 5 ngày
        */
ALTER PROC GetDetailsReport
    @typeBranch tinyint,
    @branchName nvarchar(100),
    @typeCondition tinyint,
    @page INT,
	@pageSize INT
AS 
BEGIN
    SELECT 
    	TotalRecords = COUNT(*) OVER(),
        LOC.SubCompanyNameVN,
        LOC.BranchName,
        ST.Name as Status, 
        SS.CreateDate,
        SS.EndDate
    FROM DBO.WorkingSession AS SS
		JOIN DBO.Location AS LOC ON SS.LocationID = LOC.LocationID AND SS.BranchCode = LOC.BranchCode
		JOIN DBO.Status AS ST ON SS.Status = ST.ID
    WHERE 
        CASE 
            WHEN @typeBranch = 1 THEN LOC.SubCompanyNameVN 
            WHEN @typeBranch = 2 THEN LOC.BranchName 
        END = @branchName AND 
        ((@typeCondition = 0) OR (@typeCondition = 1 AND ST.ID IN (5,6)) OR (@typeCondition = 2 AND ST.ID IN (3)) OR 
         (@typeCondition = 3 AND ST.ID IN (8,9) AND (DATEDIFF(day, SS.CreateDate, SS.EndDate) 
            - DateDiff(ww, SS.CreateDate, SS.EndDate)) <= 5) OR
         (@typeCondition = 4 AND ST.ID IN (8,9) AND (DATEDIFF(day, SS.CreateDate, SS.EndDate) 
            - DateDiff(ww, SS.CreateDate, SS.EndDate)) > 5))
    ORDER BY LOC.SubCompanyNameVN
    OFFSET (@page - 1) * @pageSize ROWS 
    FETCH NEXT @pageSize ROWS ONLY
END

EXEC GetDetailsReport 2, 'HNI_02', 0, 1, 10