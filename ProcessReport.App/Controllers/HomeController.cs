﻿using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using ProcessReport.Data.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ProcessReport.App.Controllers
{
    public class HomeController : Controller
    {
        private static HttpClient hc = new HttpClient();
        string BaseAddress = "http://localhost:57064";

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult GetReport(int page, int pageSize)
        {
            string url = BaseAddress + $"/api/Home?page={page}&pageSize={pageSize}";
            var res = hc.GetAsync(url);
            res.Wait();
            if (res.Result.IsSuccessStatusCode == false)
                return Json(null, JsonRequestBehavior.AllowGet);
            else
            {
                var Report = res.Result.Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<ReportsViewModel>>(Report), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetDetailsReport(int typeBranch, string branchName, int typeCondition, int page, int pageSize)
        {

            string url = "http://localhost:57064" + $"/api/Home?typeBranch={typeBranch}&branchName={branchName}&typeCondition={typeCondition}"
                + $"&page={page}&pageSize={pageSize}";
            var res = hc.GetAsync(url);
            res.Wait();
            if (res.Result.IsSuccessStatusCode == false)
                return Json(null, JsonRequestBehavior.AllowGet);
            else
            {
                var Report = res.Result.Content.ReadAsStringAsync().Result;
                return Json(JsonConvert.DeserializeObject<List<ReportDetailViewModel>>(Report), JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ExportToExcel(List<ReportsViewModel> data)
        {
            ExcelPackage excel = new ExcelPackage();
            var workSheet = excel.Workbook.Worksheets.Add("Report");
            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;
            //Merge header cell
            workSheet.Cells["A1:A3"].Merge = true;
            workSheet.Cells["B1:B3"].Merge = true;
            workSheet.Cells["C1:D1"].Merge = true;
            workSheet.Cells["C2:C3"].Merge = true;
            workSheet.Cells["D2:D3"].Merge = true;
            workSheet.Cells["E2:E3"].Merge = true;
            workSheet.Cells["F2:F3"].Merge = true;
            workSheet.Cells["E1:F1"].Merge = true;
            workSheet.Cells["G1:J1"].Merge = true;
            workSheet.Cells["G2:H2"].Merge = true;
            workSheet.Cells["I2:J2"].Merge = true;
            //Header of table  
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;
            workSheet.Row(2).Style.Font.Bold = true;
            workSheet.Row(3).Style.Font.Bold = true;

            workSheet.Cells[1, 1].Value = "Vùng/Chi nhánh";
            workSheet.Cells[1, 2].Value = "Tổng";
            workSheet.Cells[1, 3].Value = "Chưa xử lý";
            workSheet.Cells[1, 5].Value = "Đang xử lý";
            workSheet.Cells[1, 7].Value = "Xử lý hoàn tất";

            workSheet.Cells[2, 3].Value = "Số lượng";
            workSheet.Cells[2, 4].Value = "Tỷ lệ";
            workSheet.Cells[2, 5].Value = "Số lượng";
            workSheet.Cells[2, 6].Value = "Tỷ lệ";
            workSheet.Cells[2, 7].Value = "Trong 5 ngày";
            workSheet.Cells[2, 9].Value = "Quá 5 ngày";

            workSheet.Cells[3, 7].Value = "Số lượng (1)";
            workSheet.Cells[3, 9].Value = "Tỷ lệ";
            workSheet.Cells[3, 9].Value = "Số lượng (2)";
            workSheet.Cells[3, 10].Value = "Tỷ lệ";
            //  

            //Body of table  
            //  
            int recordIndex = 4;
            foreach (var item in data)
            {
                workSheet.Cells[recordIndex, 1].Value = item.SubCompanyOrBranchName;
                workSheet.Cells[recordIndex, 2].Value = item.Total;
                workSheet.Cells[recordIndex, 3].Value = item.CountNotProcessedYet;
                workSheet.Cells[recordIndex, 4].Value = item.PercentNotProcessedYet;
                workSheet.Cells[recordIndex, 5].Value = item.CountProcessing;
                workSheet.Cells[recordIndex, 6].Value = item.PercentProcessing;
                workSheet.Cells[recordIndex, 7].Value = item.CountProcessedIn5Days;
                workSheet.Cells[recordIndex, 8].Value = item.PercentProcessedIn5Days;
                workSheet.Cells[recordIndex, 9].Value = item.CountProcessedOver5Days;
                workSheet.Cells[recordIndex, 10].Value = item.PercentProcessedOver5Days;
                recordIndex++;
            }

            workSheet.Column(1).AutoFit();
            workSheet.Column(2).AutoFit();
            workSheet.Column(3).AutoFit();
            workSheet.Column(4).AutoFit();
            workSheet.Column(5).AutoFit();
            workSheet.Column(6).AutoFit();
            workSheet.Column(7).AutoFit();
            workSheet.Column(8).AutoFit();
            workSheet.Column(9).AutoFit();
            workSheet.Column(10).AutoFit();

            Session["DownloadExcel_FileManager"] = excel.GetAsByteArray();
            return Json("", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Download()
        {

            if (Session["DownloadExcel_FileManager"] != null)
            {
                byte[] data = Session["DownloadExcel_FileManager"] as byte[];
                return File(data, "application/octet-stream", "ReportByEPPlus.xlsx");
            }
            else
            {
                return new EmptyResult();
            }
        }
    }
}