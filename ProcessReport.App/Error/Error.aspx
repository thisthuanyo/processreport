﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>400 | Bad request.</title>

    <style type="text/css">
        body {
            padding: 30px 20px;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif;
            color: #727272;
            line-height: 1.6;
        }

        .container {
            max-width: 500px;
            margin: 0 auto;
        }

        h1 {
            margin: 0 0 40px;
            font-size: 60px;
            line-height: 1;
            color: #252427;
            font-weight: 700;
        }

        h2 {
            margin: 100px 0 0;
            font-size: 20px;
            font-weight: 600;
            letter-spacing: 0.1em;
            color: #A299AC;
            text-transform: uppercase;
        }

        p {
            font-size: 16px;
            margin: 1em 0;
        }
    </style>
</head>
<body>

    <div class="container">
        <h1>Error.</h1>
    </div>

</body>
</html>