﻿using Dapper;
using KafkaNet;
using KafkaNet.Model;
using Newtonsoft.Json;
using ProcessReport.Data.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;


namespace ProcessReport.API.Controllers
{
    public class HomeController : ApiController
    {
        Uri uri = new Uri("http://localhost:9092");
        string topic = "report-log";

        [HttpGet]
        public IHttpActionResult CreateReport(int page, int pageSize)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ProcessReportDB"].ToString()))
            {
                var parm = new DynamicParameters();
                parm.Add("@page", page);
                parm.Add("@pageSize", pageSize);
                try
                {
                    sqlConnection.Open();
                    var result = (List<ReportsViewModel>)
                        sqlConnection.Query<ReportsViewModel>("GetReportData", parm, commandType: CommandType.StoredProcedure);
                    sqlConnection.Close();
                    
                    string payload = JsonConvert.SerializeObject(result);
                    var sendMessage = new Thread(() => {
                        SaveLog(payload);
                    });
                    sendMessage.Start();

                    return Ok(result);
                }
                catch (Exception ex)
                {
                    var sendMessage = new Thread(() => {
                        SaveLog(ex.Message);
                    });
                    sendMessage.Start();
                    return BadRequest(ex.Message);
                }
            }
        }
        [HttpGet]
        public IHttpActionResult GetDetailsReport(int typeBranch, string branchName, int typeCondition, int page, int pageSize)
        {
            using (var sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ProcessReportDB"].ToString()))
            {
                var parm = new DynamicParameters();
                parm.Add("@typeBranch", typeBranch);
                parm.Add("@branchName", branchName);
                parm.Add("@typeCondition", typeCondition);
                parm.Add("@page", page);
                parm.Add("@pageSize", pageSize);
                try
                {
                    sqlConnection.Open();
                    var result = (List<ReportDetailViewModel>)
                        sqlConnection.Query<ReportDetailViewModel>("GetDetailsReport", parm, commandType: CommandType.StoredProcedure);
                    sqlConnection.Close();

                    string payload = JsonConvert.SerializeObject(result);
                    var sendMessage = new Thread(() => {
                        SaveLog(payload);
                    });
                    sendMessage.Start();

                    return Ok(result);
                }
                catch (Exception ex)
                {
                    var sendMessage = new Thread(() => {
                        SaveLog(ex.Message);
                    });
                    sendMessage.Start();
                    return BadRequest(ex.Message);
                }
            }
        }
        public void SaveLog(string payload)
        {
            KafkaNet.Protocol.Message msg = new KafkaNet.Protocol.Message(payload);
            var options = new KafkaOptions(uri);
            var router = new BrokerRouter(options);
            var client = new Producer(router);
            client.SendMessageAsync(topic, new List<KafkaNet.Protocol.Message> { msg }).Wait();
        }
    }
}